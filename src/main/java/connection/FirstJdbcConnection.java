package connection;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.mysql.jdbc.PreparedStatement;

public class FirstJdbcConnection {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		String url = "jdbc:mysql://localhost:3306/tp_jdbc";  
		String user = "jdbc_user";  
		String password = "jdbc";  
		
		
		try( Connection connection = DriverManager.getConnection(url,user,password);) {
			java.sql.PreparedStatement statement =connection.prepareStatement("select * from User");
			
			
			ResultSet resultSet = statement.executeQuery();
			
			while(resultSet.next()) {
				
				int id =  resultSet.getInt("id");  
				String name = resultSet.getString("name"); 
				int age = resultSet.getInt("age");  
				
				System.out.printf("user : %d %s %d", id, name, age);
				
				
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} 
	}

}
