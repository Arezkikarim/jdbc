package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import interfaces_impl.CommuneDBserviceImp;
import interfaces_impl.CommuneImportImpl;
import interfaces_impl.DepartementDBServiceImp;
import interfaces_impl.DepartementImportImpl;

public class Main {

	public static void main(String[] args) {
		
		
		// TODO Auto-generated method stub
		
		String pathCommune = "files/laposte_hexasmal.csv";
		String pathDepartement = "files/departement.csv";
		
		String url = "jdbc:mysql://localhost:3306/db1";
		String user = "db1_user";
		String password = "db1_user_1";
		
		
		try (Connection connection = DriverManager.getConnection(url, user, password);) {
			
			CommuneDBserviceImp communeDBServiceImp = new CommuneDBserviceImp(connection);
			DepartementDBServiceImp departementDBServiceImplement = new DepartementDBServiceImp(connection);
			try {
			
				CommuneImportImpl communeImporterImp = new CommuneImportImpl(communeDBServiceImp);
				communeImporterImp.importCommune(pathCommune);
				
				DepartementImportImpl departementImporterImp = new DepartementImportImpl(departementDBServiceImplement);
				departementImporterImp.importDepartement(pathDepartement);

			} catch (Exception ex) {
				ex.printStackTrace();
			}

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		
		
		
		

	}

}
