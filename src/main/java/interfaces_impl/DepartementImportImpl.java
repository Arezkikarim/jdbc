package interfaces_impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


import org.arezki.interfaces.DepartementImport;

import departement.Departement;

public class DepartementImportImpl implements DepartementImport {
	
	
	private DepartementDBServiceImp departementDBServiceImp;

	

	public DepartementImportImpl(DepartementDBServiceImp departementDBServiceImplement) {
		this.departementDBServiceImp = departementDBServiceImplement;
	}
	
	
	
	public DepartementImportImpl() {

	}
	
	
	
	
	Function<String, Departement> line_to_Departement = line -> {
		Departement departement = new Departement();
		String getChamp[] = line.split(";");
		
		departement.setCodePostal(getChamp[0]);
		departement.setNomDepartement(getChamp[1]);
		
		
		return departement;
	};
	
	
	private List<String> Communes_list(String fileName) {
		
		
		List<String> commune_list = new ArrayList<String>();
		
		
		Path path = Path.of(fileName);
		
		
		try (Stream<String> lines = Files.lines(path)) {

			commune_list = lines.skip(1).collect(Collectors.toList());
			
			
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		return commune_list;
	}

	public void importDepartement(String path) {
		
		List<String> lines = Communes_list(path);
		lines.forEach(e -> {
			Departement departement = line_to_Departement.apply(e);
			departementDBServiceImp.writeDepartement(departement);
		});
		// TODO Auto-generated method stub
		
	}

}
