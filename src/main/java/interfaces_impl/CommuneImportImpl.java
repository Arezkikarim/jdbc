package interfaces_impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.arezki.interfaces.CommuneImport;

import commune.Commune;

public class CommuneImportImpl implements CommuneImport {

	private CommuneDBserviceImp communeDBServiceImp;
	
	public CommuneImportImpl(CommuneDBserviceImp communeDBServiceImp) {
		this.communeDBServiceImp = communeDBServiceImp;
	}

	public CommuneImportImpl() {
		
	}
	
	
	
	
	Function<String, Commune> line_to_Commune = line -> {
		Commune commune = new Commune();
		String getChamp[] = line.split(";");
		commune.setCodeINSEE(getChamp[0]);
		commune.setNomCommune(getChamp[1]);
		commune.setCodePostal(getChamp[2]);
		commune.setLibelleAcheminement(getChamp[4]);
		return commune;
	};
	
	
	private List<String> Communes_list(String fileName) {
		
		
		List<String> commune_list = new ArrayList<String>();
		
		
		Path path = Path.of(fileName);
		
		
		try (Stream<String> lines = Files.lines(path)) {

			commune_list = lines.skip(1).collect(Collectors.toList());
			
			
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		return commune_list;
	}

	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	public void importCommune(String path) {
		// TODO Auto-generated method stub
		
		List<String> lines = Communes_list(path);
		lines.forEach(e -> {
			Commune commune = line_to_Commune.apply(e);
			communeDBServiceImp.writeCommune(commune);
		});
		
		
		
	}

}
