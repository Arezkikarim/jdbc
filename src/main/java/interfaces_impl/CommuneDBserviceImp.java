package interfaces_impl;

import java.sql.Connection;
import java.sql.PreparedStatement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.arezki.interfaces.CommuneDBService;

import commune.Commune;

public class CommuneDBserviceImp implements CommuneDBService {
	
	
	private Connection connection;

	public CommuneDBserviceImp(Connection connection) {
		this.connection = connection; 
	}
	
	

	public void writeCommune(Commune commune) {
		
		
		try(PreparedStatement preparedStatement = 
				connection.prepareStatement("INSERT INTO communes(codeINSEE, nomCommune, codePostal,libelleAcheminement) "
						+ "VALUES(?,?,?,?)", Statement.RETURN_GENERATED_KEYS);){
			
			preparedStatement.setString(1, commune.getCodeINSEE());
			preparedStatement.setString(2, commune.getNomCommune());
			preparedStatement.setString(3, commune.getCodePostal());
			preparedStatement.setString(4, commune.getLibelleAcheminement());
			
			
			preparedStatement.executeUpdate(); 
			
			
			
		}catch(SQLException sqle) {
			sqle.printStackTrace();
		}
		// TODO Auto-generated method stub
		
	}

	public Commune getCommuneById(long id) {
		Commune Commune = new Commune();
		 
		try(PreparedStatement statement = 
					 connection.prepareStatement("select id, codeINSEE, nomCommune, libelleAcheminement, "
								+ "codePostal from communes where id = ?")){
			
			
			statement.setLong(1, id);
			ResultSet resultset = 	statement.executeQuery();
			
			
			while(resultset.next()) {
				long id_commune =resultset.getLong("id");
				String codeINSEE = resultset.getString("codeINSEE");  
				String name_Commune = resultset.getString("nomCommune"); 
				String codePostal = resultset.getString("codePostal");  
				String libelleAcheminement = resultset.getString("libelleAcheminement");
				
				
				
				
				Commune.setId(id_commune);
				Commune.setCodeINSEE(codeINSEE);
				Commune.setNomCommune(name_Commune);
				Commune.setCodePostal(codePostal);
				Commune.setLibelleAcheminement(libelleAcheminement);				
			}
			
		} catch (SQLException e) {	
			e.printStackTrace();
		}
		
		return Commune;  
		// TODO Auto-generated method stub
	}

	public Commune getCommuneByName(String name) {
		
		
		Commune Commune = new Commune(); 
		try(PreparedStatement statement = 
					 connection.prepareStatement(	"select id, codeINSEE, nomCommune, libelleAcheminement, "
								+ "codePostal from communes where nomCommune like '%?%' ")){
			
			statement.setString(1, name);
			
			ResultSet resultset = 	statement.executeQuery();
			
			
			
			while(resultset.next()) {
				Commune.setId(resultset.getLong("id"));
				Commune.setCodeINSEE(resultset.getString("codeINSEE"));
				Commune.setNomCommune(resultset.getString("nomCommune"));
				Commune.setCodePostal(resultset.getString("codePostal"));
				Commune.setLibelleAcheminement(resultset.getString("libelleAcheminement"));				
			}
			
		} catch (SQLException e) {	
			e.printStackTrace();
		}
		return Commune; 
		
		
		
		
	}
	
	
	
	

	public int countCommuneByCP(String codePostal) {
		
		int count = 0;
		
		try(PreparedStatement statement = 
				 connection.prepareStatement(	"select COUNT(*) AS rowcount "
							+ "from communes where codePostal like '?%' ")){
		
		statement.setString(1, codePostal);
		
		ResultSet resultset = 	statement.executeQuery();
		resultset.next();
		count=resultset.getInt("rowcount");
		resultset.close();
					
		
		
	} catch (SQLException e) {	
		e.printStackTrace();
	}
		
		
		// TODO Auto-generated method stub
		return count;
	}

}
