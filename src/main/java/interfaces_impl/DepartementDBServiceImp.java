package interfaces_impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


import org.arezki.interfaces.DepartementDBService;

import departement.Departement;

public class DepartementDBServiceImp implements DepartementDBService {

	
	
	
	
	
	
	
	
	
	
	private Connection connection; 
	
	public DepartementDBServiceImp(Connection connection) {
		this.connection = connection; 
	}
	
	
	
	public void writeDepartement(Departement departement) {
		
		
		try(PreparedStatement preparedStatement = 
				connection.prepareStatement("INSERT INTO departement(codePostal, nomDepartement) "
						+ "VALUES(?,?)",Statement.RETURN_GENERATED_KEYS);){
			
			preparedStatement.setString(1, departement.getCodePostal());
			
			preparedStatement.setString(2, departement.getNomDepartement());
			
			preparedStatement.executeUpdate(); 
			
			 try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                    if (resultSet.next()) {
                    	departement.setCodePostal(resultSet.getString(1));
                    }
                } catch (SQLException s) {
                    s.printStackTrace();
                }
			
		}catch(SQLException sqle) {
			sqle.printStackTrace();
		}
		// TODO Auto-generated method stub
		
	}
	
	
	
	

	public Departement getDepartementByCP(String codePostal) {
		
		
		Departement Departement = new Departement(); 
		try(PreparedStatement statement = 
					 connection.prepareStatement("select id, codePostal, nomDepartement "
								+ "from departement where codePostal = ?")){	
			statement.setString(1,codePostal);  
			ResultSet resultset = 	statement.executeQuery();
			while(resultset.next()) {
				Departement.setCodePostal(resultset.getString("codePostal"));
				Departement.setNomDepartement(resultset.getString("nomDepartement"));
						
			}
			
		} catch (SQLException e) {	
			e.printStackTrace();
		}
		return Departement;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public Departement getDepartementByName(String name) {
		// TODO Auto-generated method stub
		Departement Departement = new Departement(); 
		
		
		try(PreparedStatement statement = 
					 connection.prepareStatement("select id, codePostal, nomDepartement from departement "
							 + "where nomDepartement like '?'")){	
			statement.setString(1, name);  
			ResultSet resultset = 	statement.executeQuery();
			while(resultset.next()) {
				Departement.setCodePostal(resultset.getString("codePostal"));
				Departement.setNomDepartement(resultset.getString("nomDepartement"));
						
			}
			
		} catch (SQLException e) {	
			e.printStackTrace();
		}
		return Departement;  
	}

	public int countCommuneByCP(String codePostal) {
		// TODO Auto-generated method stub
		
		
		
		return 0;
	}

}
