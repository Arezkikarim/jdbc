package serie_1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CommuneAnalyser {
	
	public static void main(String[] args)  {
		
		String fileName = "files/laposte_hexasmal.csv";
		Path path = Path.of(fileName);
		List<String> communeCSV = null;
		try {
			communeCSV = Files.lines(path).skip(1).collect(Collectors.toList());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
		

		Map<String, Long> collection = communeCSV.stream()
				  .map(lines -> lines.split(";")[0])
				  .collect(Collectors.groupingBy(
						  Function.identity(), Collectors.counting()
				  ));  
		
		collection.entrySet().removeIf(entry -> entry.getValue() == 1);  
		collection.forEach((codePostal,value) -> System.out.printf("%s %d \n",codePostal, value ));
		
	}
	
	
	
	
	
	

}
